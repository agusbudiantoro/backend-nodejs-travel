'use strict';

var response = require('./res');
var connection = require('./koneksi');

exports.index = function(req, res){
    res.status(200).send("Aplikasi res API Berjalan");
    // response.ok("Aplikasi rest api ku berjalan",res)
};


exports.tampilAll = function(req, res){
    connection.query("SELECT * FROM user", function(error, rows){
        if(error){
            res.status(401).send({message:"error"})
        }else {
            if(rows.length != 0){
                res.status(200).send({message:rows});
            } else {
                res.status(404).send({message:"data kosong"});
            }
        }
    })
}

// exports.tampilMahasiswa = function(req, res){
//     connection.query("SELECT * FROM user",function(error, rows, fields){
//         if(error){
//             res.status(401).send(error);
//         } else {
//             if(rows.length != 0){
//                 res.status(200).send({value:rows});
//             } else{
//                 res.status(404).send({message:"data kosong"});
//             }
//         }
//     })
// }

//menampilkan semua data warga
// exports.tampilsemuawarga = function(req, res){
//     connection.query('SELECT * FROM warga', function(error, rows, fileds){
//         if(error){
//             console.log(error);
//         }else {
//             response.ok(rows, res)
//         }
//     });
// };

// //menampilkan data warga berdasarkaan id
// exports.tampilberdasarkanid = function(req,res){
//     let id = req.params.id;
//     connection.query('SELECT * FROM warga WHERE id = ?', [id],
//         function (error, rows, fields) {
//             if(error){
//                 console.log(error);
//             }else {
//                 response.ok(rows, res);
//             }
//     });
// };

// //menambahkan data warga
// exports.tambahdatawarga = function(req,res){
//     var nama = req.body.nama;
//     var no_ktp = req.body.no_ktp;
//     var usia = req.body.usia;
//     var jenis = req.body.jenis_kelamin;
//     var kondisi = req.body.kondisi;

//     connection.query('INSERT INTO warga (nama,no_ktp,usia,jenis_kelamin,kondisi) VALUES(?,?,?,?,?)',
//     [nama,no_ktp,usia,jenis,kondisi],
//     function(error, rows, fields){
//         if(error){
//             console.log(error);
//         }else {
//             response.ok("berhasil menambahkan data!",res)
//         }
//     });
// };

// //mengubah data warga berdasarkan id
// exports.ubahWarga = function(req,res){
//     var id_warga = req.body.id;
//     var nama = req.body.nama;
//     var no_ktp = req.body.no_ktp;
//     var usia = req.body.usia;
//     var jenis = req.body.jenis_kelamin;
//     var kondisi = req.body.kondisi;

//     connection.query('UPDATE warga SET nama=?, no_ktp=?, usia=?, jenis_kelamin=?, kondisi=? WHERE id=?', [nama, no_ktp, usia, jenis, kondisi, id_warga],
//         function(error, rows, fields){
//             if(error){
//                 console.log(error);
//             }else{
//                 response.ok("berhasil ubah data", res)
//             }
//         });
// };

// //delete data warga
// exports.deletewarga = function(req,res){
//     var id_warga = req.body.id;
//     connection.query(' DELETE FROM warga WHERE id=?',[id_warga],
//     function(error, rows, fields){
//         if(error){
//             console.log(error);
//         }else{
//             response.ok("berhasil hapus data", res)
//         }
//     });
// }

// //menampilkan nama warga group
// exports.tampilgroupnama = function(req, res){
//     connection.query('SELECT kartu_keluarga.id, kartu_keluarga.no_kk, warga.nama FROM relasi_kk JOIN kartu_keluarga JOIN warga WHERE relasi_kk.id_warga = warga.id AND relasi_kk.id_kk = kartu_keluarga.id ORDER BY `kartu_keluarga`.`id`',
//         function(error, rows, fields){
//             if(error){
//                 console.log(error);
//             } else{
//                 response.oknested(rows, res);
//             }
//         }
//     )
// }
const express = require('express');
const multer = require("multer");
const path = require("path");
const router = express.Router();

const auth = require('./auth');
const verifikasi = require('./verifikasi');
const barang = require('./controller/tabel_barang');
const keranjang = require('./controller/tabel_keranjang');
const pembayaran = require('./controller/tabel_transaksi');
const transaksi = require('./controller/tabel_riwayat_pesanan');
const statusTransaksi= require('./controller/tabel_status');
const kategori = require('./controller/tabel_kategori');
const metodePembayaran = require('./controller/tabel_metode_pembayaran');

const storage = multer.diskStorage({
    destination: './upload/images_profil',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});
const storageBarang = multer.diskStorage({
    destination: './upload/images_barang',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

const storageLogoPembayaran = multer.diskStorage({
    destination: './upload/images_logo_pembayaran',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 10000000 }
})
var uploadBarang = multer({
    storage: storageBarang,
    limits: { fileSize: 10000000 }
})
var uploadLogoPembayaran = multer({
    storage: storageLogoPembayaran,
    limits: { fileSize: 10000000 }
})

router.use(express.static('public'));
router.use('/foto', express.static('../upload/images_profil'),(req,res)=>{
    console.log("cek");
});

//cektokenadmin
router.post('/api/v1/cekOauthToken', verifikasi.verifikasiGeneral());

//cektokenuser
router.post('/api/v1/cekOauthTokenUser', verifikasi.verifikasiUser(),auth.cekToken);

// barang
router.post('/api/v2/tambahBarang',uploadBarang.single('gambar'),barang.tambahBarang);
router.get('/api/v2/tampilBarang',barang.tampilBarang);
router.get('/api/v2/tampilBarangRekomen',barang.tampilBarangRekomendasi);
router.get('/api/v2/tampilBarangPromo',barang.tampilBarangPromo);
router.get('/api/v2/tampilBarangNoPromo',barang.tampilBarangNoPromo);
router.get('/api/v2/tampilBarangByKategori',barang.tampilBarangByKategori);
router.delete('/api/v2/hapusBarang',barang.hapusBarang);
router.put('/api/v2/editBarang',uploadBarang.single('gambar'),barang.editBarang);

// keranjang
router.post('/api/v2/tambahBarangKeranjang',keranjang.tambahBarangInKeranjang);
router.delete('/api/v2/hapusBarangKeranjang',keranjang.hapusBarangInKeranjang);
router.post('/api/v2/tampilBarangKeranjang',keranjang.tampilBarangKeranjangByIdKonsumen);

// pembayaran
router.get('/api/v2/lihatStatusTransaksi',pembayaran.lihatStatusTransaksi);
router.post('/api/v2/postTransaksi',transaksi.tambahRiwayatPesanan);
router.post('/api/v2/getPesananByIdKonsumen',transaksi.tampilPesananByIdKonsumen);
router.get('/api/v2/getAllPesanan',transaksi.tampilAllPesanan);
router.post('/api/v2/postLogoPembayaran',uploadLogoPembayaran.single('logo'));
router.get('/api/v2/getMetodePembayaran',metodePembayaran.tampilMetodePembayaran);
router.delete('/api/v2/hapusPesanan',transaksi.hapusRiwayatPesanan);
router.put('/api/v2/editStatusPesanan',transaksi.updateStatusPesanan);

// kategori
router.get('/api/v2/getkategori', kategori.tampilKategori);

// status
router.post('/api/v2/getStatusByIdPemesanan', statusTransaksi.tampilStatus);
router.put('/api/v2/updateStatus', statusTransaksi.updateStatus);

//auth
// router.post('/api/v1/register', auth.cekAkun(),upload.single('foto'),auth.register);
router.post('/api/v1/register', auth.cekAkun(),auth.register);
router.put('/api/v1/editPass', verifikasi.verifikasiGeneral(),auth.gantiPass);
router.post('/api/v1/login', auth.login);

router.post('/api/v1/', upload.single('foto'), auth.cek);

module.exports = router;
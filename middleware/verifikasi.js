const jwt = require('jsonwebtoken');
const config = require('../config/secret');
var auth = require('./auth');
var connection = require('../koneksi');
const response = require('../res');
var mysql = require('mysql');

exports.verifikasiAdmin = function verifikasi(){
    return function(req, rest, next){
        // cek token authorization header
        console.log("verif token");
        // console.log(req.headers.authorization);
        var tokenWithBearer = req.headers.authorization;
        console.log(tokenWithBearer);
        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];
            //verif
            jwt.verify(token, config.secret, function(err, decode){
                if(err){
                    return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
                }else{
                    //cari token
                    var get = {
                        access_token: token
                    }
                    console.log(`${get.access_token}`);
                    //mencari token yg sama
                    var query = "SELECT * FROM ?? WHERE ??=?";
                    var table = ["akses_token", "access_token", get.access_token];
                    query = mysql.format(query,table);
                    connection.query(query,function(error,rows){
                        console.log("cek error dan rows");
                        if(error){
                            console.log(error);
                        }else{
                            console.log(rows);
                            console.log("cek dicari token");
                            if(rows.length != 0){
                                req.auth = decode;
                                    next();
                            }else{
                                return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                            }
                        }
                    });
                }
            });
        }else{
            return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
        }
    }
}

exports.verifikasiUser = function verifikasi(){
    return function(req, rest, next){
        // cek token authorization header
        console.log("verif token");
        // console.log(req.headers.authorization);
        var tokenWithBearer = req.headers.authorization;
        console.log(tokenWithBearer);
        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];
            //verif
            jwt.verify(token, config.secret, function(err, decode){
                if(err){
                    return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
                }else{
                    //cari token
                    var get = {
                        access_token: token
                    }
                    console.log(`${get.access_token}`);
                    //mencari token yg sama
                    var query = "SELECT * FROM ?? WHERE ??=?";
                    var table = ["akses_token", "access_token", get.access_token];
                    query = mysql.format(query,table);
                    connection.query(query,function(error,rows){
                        console.log("cek error dan rows");
                        if(error){
                            console.log(error);
                        }else{
                            if(rows.length != 0){
                                if(rows[0].role == 1){
                                    req.auth = decode;
                                    next();
                                }else{
                                    return rest.status(401).send({auth:false,message:"gagal mengotorisasi role anda!"});
                                }
                            }else{
                                return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                            }
                        }
                    });
                }
            });
        }else{
            return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
        }
    }
}

exports.verifikasiGeneral = function verifikasi(){
    return function(req, rest, next){
        // cek token authorization header
        // console.log(req.headers.authorization);
        var tokenWithBearer = req.headers.authorization;
        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];
            //verif
            jwt.verify(token, config.secret, function(err, decode){
                if(err){
                    return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
                }else{
                    //cari token
                    var get = {
                        access_token: token
                    }
                    //mencari token yg sama
                    var query = "SELECT * FROM ?? WHERE ??=?";
                    var table = ["akses_token", "access_token", get.access_token];
                    query = mysql.format(query,table);
                    connection.query(query,function(error,rows){
                        if(error){
                            console.log(error);
                        }else{
                            if(rows.length != 0){
                                    req.auth = decode;
                                    return next();
                            }else{
                                return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                            }
                        }
                    });
                }
            });
        }else{
            return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
        }
    }
}

// module.exports = verifikasi;
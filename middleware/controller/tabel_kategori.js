const connection = require('../../koneksi');
const mysql = require('mysql');
const response = require('../../res');

// tampil semua kategori
exports.tampilKategori = function (req, res) {
    var query = "SELECT * FROM ??";
    var table = ["kategori"];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan kategori" })
        } else {
            console.log("cek barang");
            response.ok(rows, res);
        }
    });
}